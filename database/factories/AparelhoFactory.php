<?php

use Faker\Generator as Faker;

$factory->define(\App\Aparelho::class, function (Faker $faker) {
    return [
        'descricao_aparelho' => $faker->text(15),
        'codigo_aparelho' => rand(1,5)
    ];
});
