<?php

use Faker\Generator as Faker;

$factory->define(\App\Usuario::class, function (Faker $faker) {
    return [
        'nome_usuario' => $faker->firstName,
        'email' => $faker->companyEmail,
        'login' => $faker->lastName,
        'senha' => md5($faker->randomNumber(4)),
        'data_criacao' => $faker->dateTime('now','UTC'),
        'tempo_expiracao_senha' => $faker->randomNumber(4),
        'cod_autorizacao' => $faker->randomNumber(1),
        'status_usuario' => $faker->randomElement(['A', 'I']),
        'cod_pessoa' => $faker->numerify('#####')
    ];
});
