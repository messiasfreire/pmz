<?php

use Illuminate\Database\Seeder;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Usuario::class, 30)->create()
            ->each(function ($u) {
            $u->perfis()->attach(App\Perfil::all('id_perfil')->random(2));
            $u->aparelhos()->attach(App\Aparelho::all('id_aparelho')->random(10));
        });
    }
}
