<?php

use Illuminate\Database\Seeder;

class PerfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Perfil::class)->create(['nome_perfil' => 'cliente']);
        factory(App\Perfil::class)->create(['nome_perfil' => 'administrador']);
    }
}
