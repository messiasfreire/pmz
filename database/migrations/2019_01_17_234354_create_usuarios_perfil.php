<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosPerfil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios_perfil', function (Blueprint $table) {
            $table->integer('id_usuario');
            $table->integer('id_perfil');

            $table->foreign('id_usuario')->references('id_usuario')->on('usuarios');
            $table->foreign('id_perfil')->references('id_perfil')->on('perfis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios_perfil');
    }
}
