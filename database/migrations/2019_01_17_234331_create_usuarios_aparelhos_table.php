<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosAparelhosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios_aparelhos', function (Blueprint $table) {
            $table->integer('id_usuario');
            $table->integer('id_aparelho');

            $table->foreign('id_usuario')->references('id_usuario')->on('usuarios');
            $table->foreign('id_aparelho')->references('id_aparelho')->on('aparelhos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios_aparelhos');
    }
}
