## PROJETO PEMAZA

### Configurações do projeto:
## [O projeto foi desenvolvido Usando Laradock](http://laradock.io/)

1 Inicializando o Projeto . faça o clone do projeto 

Para rodar o ambiente é necessário instalar o NodeJS o (Vuejs)
Apache2 ou Nginx para rodar o Backend (Laravel)

Se usar Docker , o Laradock a trás containers para agilizar a instalacao/ config é so clicar [aqui](http://laradock.io/)

- composer install ( Instalação do Dependencia do Laravel)
- npm i     (Instalação Dependencias do Node)
- npm run dev 
- cp env.example .env (Copiar o exemplo do env )
- configurar o .env de acordo com o seu ambiente
- php artisan key:generate
- Rodar as migrations com as seeders: php artisan migrate --seed



Projeto 

* Foi usado Framework VueJS e a biblioteca de compentente [Vuetify](https://vuetifyjs.com/pt-BR/)

