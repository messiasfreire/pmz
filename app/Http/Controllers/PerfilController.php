<?php

namespace App\Http\Controllers;

use App\Services\PerfilService;

class PerfilController extends Controller
{
    /**
     * @var PerfilService
     */
    private $perfilService;

    public function __construct(PerfilService $perfilService)
    {
        $this->perfilService = $perfilService;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $perfis = $this->perfilService->getAll();

        if ($perfis->count() == 0) {
            return Response()->json([
                'success' => false,
                'message' => 'Não foram encontrado registro de perfis.'
            ], 404);
        }

        return Response()->json([
            'success' => true,
            'data' => $perfis
        ], 200);
    }
}
