<?php

namespace App\Http\Controllers;

use App\Exports\UsuarioAparelhosExport;
use App\Exports\UsuarioPerfisExport;
use App\Http\Requests\UsuarioRequest;
use App\Http\Requests\UsuarioUpdateRequest;
use App\Services\UsuarioService;
use Maatwebsite\Excel\Facades\Excel;

class UsuarioController extends Controller
{

    /**
     * @var UsuarioService
     */
    private $usuarioService;

    public function __construct(UsuarioService $usuarioService)
    {
        $this->usuarioService = $usuarioService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('usuario.index');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $usuarios = $this->usuarioService->getAll();

        if ($usuarios->count() == 0) {
            return Response()->json([
                'success' => false,
                'message' => 'Não foram encontrado registro de usuários.'
            ], 404);
        }

        return Response()->json([
            'success' => true,
            'data' => $usuarios
        ], 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getById($id)
    {
        $usuario = $this->usuarioService->getById($id);

        if ($usuario == null) {
            return Response()->json([
                'success' => false,
                'message' => 'Não foi possivel encontrar o registro do usuário.'
            ], 404);
        }

        return Response()->json([
            'success' => true,
            'data' => $usuario
        ], 200);
    }

    /**
     * Cadastra um novo usuario
     *
     * @param UsuarioRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UsuarioRequest $request)
    {
        $data = $request->all();

        $response = $this->usuarioService->create($data);

        if (isset($response['success']) && $response['success'] === false) {
            return Response()->json([
                'success' => false,
                'message' => 'Não foi possivel salvar o registro do usuário.',
                'error' => $response['error']
            ], 400);
        }

        return Response()->json($response, 200);
    }

    /**
     * Atualiza o registro do usuario
     *
     * @param UsuarioUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UsuarioUpdateRequest $request, $id)
    {
        $data = $request->all();

        $response = $this->usuarioService->update($data, $id);

        if (isset($response['success']) && $response['success'] === false) {
            return Response()->json([
                'success' => false,
                'message' => 'Não foi possível atualizar o registro do usuário.',
                'error' => $response['error']
            ], 400);
        }

        return Response()->json($response, 200);
    }

    /**
     * Deleta o usuario.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $response = $this->usuarioService->delete($id);

        if (isset($response['success']) && $response['success'] === false) {
            return Response()->json([
                'success' => false,
                'message' => 'Não foi possível apagar o registro do usuário.',
                'error' => $response['error']
            ], 400);
        }

        return Response()->json($response, 200);
    }

    /**
     * Restaura o usuario
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore($id)
    {
        $response = $this->usuarioService->restoreUsuario($id);

        if (isset($response['success']) && $response['success'] === false) {
            return Response()->json([
                'success' => false,
                'message' => 'Não foi possível restaurar o registro do usuário.'
            ], 400);
        }

        return Response()->json($response, 200);
    }

    /**
     * Exporta o pdf de perfis do usuário
     *
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function exportPerfisPDF($id)
    {
        $response = $this->usuarioService->exportPDF($id, 'perfil');

        if (is_array($response) && isset($response['success']) && $response['success'] == false) {
            return Response()->json([
                'success' => false,
                'message' => $response['message']
            ], 500);
        }

        return response($response->Output(), 200)->header('Content-Type', 'application/pdf');
    }

    /**
     * Exporta o aparelhos de perfis do usuário
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function exportAparelhosPDF($id)
    {
        $response = $this->usuarioService->exportPDF($id, 'aparelho');

        if (is_array($response) && isset($response['success']) && $response['success'] == false) {
            return Response()->json([
                'success' => false,
                'message' => $response['message']
            ], 500);
        }

        return response($response->Output(), 200)->header('Content-Type', 'application/pdf');
    }

    /**
     * Exporta o txt de perfis do usuário
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function exportPerfisTxt($id)
    {
        $usuario = $this->usuarioService->getById($id);

        if ($usuario == null) {
            return Response()->json([
                'success' => false,
                'message' => 'Não foi possivel encontrar o registro do usuário.'
            ], 404);
        }

        if (count($usuario->perfis) == 0) {
            return Response()->json([
                'success' => false,
                'message' => 'Não existem perfis associados ao usuário.'
            ], 404);
        }

        $content = \View::make('txt.perfis')->with('usuario', $usuario);

        $headers = array(
            'Content-Type' => 'plain/txt',
            'Content-Disposition' => sprintf('attachment; filename="%s"', 'usuarios_perfis.txt'),
        );

        return Response()->make($content, 200, $headers);

    }

    /**
     * Exporta o txt dos aparelhos do usuário
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function exportAparelhosTxt($id)
    {
        $usuario = $this->usuarioService->getById($id);

        if ($usuario == null) {
            return Response()->json([
                'success' => false,
                'message' => 'Não foi possivel encontrar o registro do usuário.'
            ], 404);
        }

        if (count($usuario->aparelhos) == 0) {
            return Response()->json([
                'success' => false,
                'message' => 'Não existem aparelhos associados ao usuário.'
            ], 404);
        }

        $content = \View::make('txt.aparelhos')->with('usuario', $usuario);

        $headers = array(
            'Content-Type' => 'plain/txt',
            'Content-Disposition' => sprintf('attachment; filename="%s"', 'usuarios_aparelhos.txt'),
        );

        return Response()->make($content, 200, $headers);
    }

    /**
     * Exporta o CSV dos perfis do usuário
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportPerfisCSV($id)
    {
        $usuario = $this->usuarioService->getById($id);

        if ($usuario == null) {
            return Response()->json([
                'success' => false,
                'message' => 'Não foi possivel encontrar o registro do usuário.'
            ], 404);
        }

        if (count($usuario->perfis) == 0) {
            return Response()->json([
                'success' => false,
                'message' => 'Não existem perfis associados ao usuário.'
            ], 404);
        }

        return Excel::download(new UsuarioPerfisExport($id), 'usuarios_perfis.csv');

    }

    /**
     * Exporta o CSV dos aparelhos do usuário
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportAparelhosCSV($id)
    {
        $usuario = $this->usuarioService->getById($id);

        if ($usuario == null) {
            return Response()->json([
                'success' => false,
                'message' => 'Não foi possivel encontrar o registro do usuário.'
            ], 404);
        }

        if (count($usuario->aparelhos) == 0) {
            return Response()->json([
                'success' => false,
                'message' => 'Não existem aparelhos associados ao usuário.'
            ], 404);
        }

        return Excel::download(new UsuarioAparelhosExport($id), 'usuarios_aparelhos.csv');
    }
}
