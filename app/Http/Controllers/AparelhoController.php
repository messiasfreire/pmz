<?php

namespace App\Http\Controllers;

use App\Services\AparelhoService;

class AparelhoController extends Controller
{

    /**
     * @var AparelhoService
     */
    private $aparelhoService;

    public function __construct(AparelhoService $aparelhoService)
    {
        $this->aparelhoService = $aparelhoService;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $aparelhos = $this->aparelhoService->getAll();

        if ($aparelhos->count() == 0) {
            return Response()->json([
                'success' => false,
                'message' => 'Não foram encontrado registro de aparelhos.'
            ], 404);
        }

        return Response()->json([
            'success' => true,
            'data' => $aparelhos
        ], 200);
    }
}
