<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nome_usuario" => "required|max:60",
            "email" => "required|max:60|email",
            "login" => "required|max:12",
            "senha" => "required|max:45",
            "cod_autorizacao" => "required|max:1",
            "status_usuario" => "required"
        ];
    }


    public function attributes()
    {
        return [
            'nome_usuario' => 'nome de usuário',
            'status_usuario' => 'registro do usuário',
            'cod_autorizacao' => 'código de autorização',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(Response()->json([
            'success' => false,
            'message' => 'Existem campos inválidos',
            'errors' => $validator->errors()->all()
        ], 422));
    }
}
