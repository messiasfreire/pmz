<?php

namespace App\Helpers;


use App\Usuario;
use Crabbly\FPDF\FPDF;

class PDFUsuario extends FPDF
{


    /**
     * Prepara para gerar o pdf dos perfis e de aparelhos do usuário.
     * @param Usuario $usuario
     * @param $tipo
     * @return PDFUsuario|bool
     */
    public function generatePdfUsuario(Usuario $usuario, $tipo)
    {
        $this->AddPage('L', 'A4', 0);
        $this->SetFont('Arial', '', 15);

        if ($tipo == 'perfil') {

            $this->Cell(190, 10, utf8_decode('Listagem de perfis do usuário: ' . $usuario->cod_pessoa . ' - ' . $usuario->nome_usuario), 1, 1, 'C');


            foreach ($usuario->perfis as $perfil) {
                $this->SetFont('Arial', '', 12);
                $this->Cell(30, 5, $perfil->id_perfil, 1, 0, 'C');

                $this->SetFont('Arial', '', 12);
                $this->Cell(160, 5, $perfil->nome_perfil, 1, 0, 'C');
            }

        } elseif ($tipo == 'aparelho') {

            $this->Cell(190, 10, utf8_decode('Listagem de aparelhos do usuário: ' . $usuario->cod_pessoa . ' - ' . $usuario->nome_usuario), 1, 1, 'C');


            foreach ($usuario->aparelhos as $aparelho) {
                $this->SetFont('Arial', '', 12);
                $this->Cell(30, 5, $aparelho->codigo_aparelho, 1, 0, 'C');

                $this->SetFont('Arial', '', 12);
                $this->Cell(160, 5, $aparelho->descricao_aparelho, 1, 0, 'L');
                $this->Ln();
            }

        } else {
            return false;
        }

        return $this;
    }
}
