<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aparelho extends Model
{
    protected $table = 'aparelhos';
    protected $primaryKey = 'id_aparelho';
    public $timestamps = false;

    protected $fillable = [
        'descricao_aparelho',
        'codigo_aparelho'
    ];


    public function usuarios()
    {
        return $this->belongsToMany(Usuario::class, 'usuarios_aparelhos','id_aparelho','id_usuario');
    }
}
