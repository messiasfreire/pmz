<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Usuario extends Model
{
    use SoftDeletes;

    protected $table = 'usuarios';
    protected $primaryKey = 'id_usuario';
    protected $dates = ['deleted_at'];
    public $timestamps = false;


    protected $fillable = [
        'nome_usuario',
        'login',
        'email',
        'senha',
        'data_criacao',
        'tempo_expiracao_senha',
        'cod_autorizacao',
        'status_usuario',
        'cod_pessoa'
    ];

    public function perfis()
    {
        return $this->belongsToMany(Perfil::class, 'usuarios_perfil', 'id_usuario', 'id_perfil');
    }

    public function aparelhos()
    {
        return $this->belongsToMany(Aparelho::class, 'usuarios_aparelhos', 'id_usuario', 'id_aparelho');
    }
}
