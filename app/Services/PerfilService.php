<?php

namespace App\Services;


use App\Perfil;

class PerfilService
{

    /**
     * @var Perfil
     */
    private $perfil;

    public function __construct(Perfil $perfil)
    {
        $this->perfil = $perfil;
    }


    /**
     * Retorna todos os perfis
     *
     * @return Perfil[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->perfil->get();
    }
}
