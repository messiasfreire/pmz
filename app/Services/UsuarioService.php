<?php

namespace App\Services;


use App\Helpers\PDFUsuario;
use App\Usuario;
use Carbon\Carbon;

class UsuarioService
{

    private $usuario;

    public function __construct(Usuario $usuario)
    {
        $this->usuario = $usuario;
    }


    /**
     * Retorna todos os usuarios
     *
     * @return Usuario[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->usuario->orderBy('id_usuario', 'desc')->get()
            ->load(['aparelhos', 'perfis']);
    }


    /**
     * Retorna o usuario do id
     *
     * @return Usuario[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getById($id)
    {
        return $this->usuario->find($id);
    }

    /**
     * Salva um novo usuario.
     *
     * @param array $data
     * @return array
     */
    public function create(array $data)
    {
        try {

            $data += [
                'data_criacao' => Carbon::now()->toDateTimeString(),
                'tempo_expiracao_senha' => rand(100, 9999),
                'cod_pessoa' => (int)substr(time(), 0, 8)
            ];

            $usuario = $this->usuario->create($data);

            if (isset($data['perfis']) && count($data['perfis']) > 0) {
                $usuario->perfis()->attach($data['perfis']);
            }

            if (isset($data['aparelhos']) && count($data['aparelhos']) > 0) {
                $usuario->aparelhos()->attach($data['aparelhos']);
            }

            return [
                'success' => true,
                'data' => $usuario
            ];
        } catch (\Throwable $exception) {
            return [
                'success' => false,
                'error' => $exception->getMessage()
            ];
        }
    }

    /**
     * Atualiza o registro do usuario
     *
     * @param array $data
     * @param $id
     * @return array
     */
    public function update(array $data, $id)
    {
        try {
            $usuario = $this->usuario->findOrFail($id);

            $usuario->fill($data)->save();

            if (isset($data['perfis']) && count($data['perfis']) > 0) {
                $usuario->perfis()->sync($data['perfis']);
            }

            if (isset($data['aparelhos']) && count($data['aparelhos']) > 0) {
                $usuario->aparelhos()->sync($data['aparelhos']);
            }

            return [
                'success' => true,
                'data' => $usuario
            ];
        } catch (\Throwable $exception) {
            return [
                'success' => false,
                'error' => $exception->getMessage()
            ];
        }
    }

    /**
     * Apaga o registro do usuario.
     *
     * @param $id
     * @return array
     */
    public function delete($id)
    {
        try {
            $usuario = $this->usuario->findOrFail($id);
            $usuario->delete();

            return [
                'success' => true
            ];
        } catch (\Throwable $exception) {
            return [
                'success' => false,
                'error' => $exception->getMessage()
            ];
        }
    }


    /**
     * Restaura um usuario excluido.
     *
     * @param $id_usuario
     * @return array
     */
    public function restoreUsuario($id_usuario)
    {
        try {
            $this->usuario->withTrashed()->where('id_usuario', $id_usuario)->restore();

            return [
                'success' => true
            ];
        } catch (\Throwable $exception) {
            return [
                'success' => false,
                'error' => $exception->getMessage()
            ];
        }
    }

    /**
     * Gera o PDF do usuário com os aparelhos ou perfis
     *
     * @param $id
     * @param $tipo
     * @return PDFUsuario|array|bool|\Illuminate\Http\JsonResponse
     */
    public function exportPDF($id, $tipo)
    {
        /** @var Usuario $usuario */
        $usuario = self::getById($id);

        if ($usuario == null) {
            return [
                'success' => false,
                'message' => 'Não foi possível encontrar o usuário.'
            ];
        }

        $pdf = new PDFUsuario();

        if($tipo == 'perfil'){

            if (count($usuario->perfis) == 0) {
                return [
                    'success' => false,
                    'message' => 'Não existem perfis associados ao usuário.'
                ];
            }

            $pdf = $pdf->generatePdfUsuario($usuario, 'perfil');

        } elseif($tipo == 'aparelho'){

            if (count($usuario->aparelhos) == 0) {
                return [
                    'success' => false,
                    'message' => 'Não existem aparelhos associados ao usuário.'
                ];
            }

            $pdf = $pdf->generatePdfUsuario($usuario, 'aparelho');
        }

        if($pdf == false){
            return [
                'success' => false,
                'message' => 'Não foi possível gerar o pdf.'
            ];
        }

        return $pdf;
    }

}
