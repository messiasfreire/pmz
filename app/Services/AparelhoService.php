<?php

namespace App\Services;


use App\Aparelho;

class AparelhoService
{

    /**
     * @var Aparelho
     */
    private $aparelho;

    public function __construct(Aparelho $aparelho)
    {
        $this->aparelho = $aparelho;
    }

    /**
     * Retorna todos os aparelhos
     *
     * @return Aparelho[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->aparelho->get();
    }
}
