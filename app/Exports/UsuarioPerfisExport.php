<?php

namespace App\Exports;

use App\Usuario;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsuarioPerfisExport implements FromCollection, WithHeadings
{

    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Usuario::query()->find($this->id)->perfis;
    }

    public function headings(): array
    {
        return [
            '#',
            'Perfil',
        ];
    }
}
