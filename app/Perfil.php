<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $table = 'perfis';
    protected $primaryKey = 'id_perfil';
    public $timestamps = false;

    protected $fillable = [
        'nome_perfil'
    ];

    public function usuarios()
    {
        return $this->belongsToMany(Usuario::class, 'usuarios_perfil','id_perfil','id_usuario');
    }
}
