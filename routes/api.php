<?php

/*
|-Rotas para simular consumo da API  -

Messias Frere
|
*/

Route::group(['prefix' => 'usuario', 'as' => 'usuario.'], function () {
    Route::get('get', 'UsuarioController@getAll')->name('get-all');
    Route::get('get/{id}', 'UsuarioController@getById')->name('get-id');
    Route::post('salvar', 'UsuarioController@store')->name('store');
    Route::put('atualizar/{id}', 'UsuarioController@update')->name('update');
    Route::delete('delete/{id}', 'UsuarioController@delete')->name('delete');
    Route::get('restore/{id}', 'UsuarioController@restore')->name('restore');

    Route::group(['prefix' => 'export'], function () {
        Route::get('perfis/pdf/{id}', 'UsuarioController@exportPerfisPDF')->name('export-perfis-pdf');
        Route::get('aparelhos/pdf/{id}', 'UsuarioController@exportAparelhosPDF')->name('export-pdf');

        Route::get('perfis/csv/{id}', 'UsuarioController@exportPerfisCSV')->name('export-perfis-csv');
        Route::get('aparelhos/csv/{id}', 'UsuarioController@exportAparelhosCSV')->name('export-aparelhos-csv');

        Route::get('perfis/txt/{id}', 'UsuarioController@exportPerfisTxt')->name('export-perfis-txt');
        Route::get('aparelhos/txt/{id}', 'UsuarioController@exportAparelhosTxt')->name('export-aparelhos-txt');
    });
});

Route::get('perfil/get', 'PerfilController@getAll')->name('perfil.get-all');
Route::get('aparelho/get', 'AparelhoController@getAll')->name('aparelho.get-all');
