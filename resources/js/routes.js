import Vue from 'vue';
import VueRouter from 'vue-router';

import Usuarios from '@/js/components/Usuarios';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/usuarios',
            name: 'usuarios',
            component: Usuarios
        }
    ]
});


export default router;
