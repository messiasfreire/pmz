import './bootstrap';
import Vue from 'vue';
import Vuetify from 'vuetify';

import Routes from '@/js/routes.js';
import App from '@/js/views/App';

import 'vuetify/dist/vuetify.min.css';


Vue.use(Vuetify, {
    theme: {
        primary: '#1D334B',
        secondary: '#607d8b',
        accent: '#2196f3',
        error: '#f44336',
        warning: '#ff9800',
        info: '#2196f3',
        success: '#4caf50'
    }
});


const app = new Vue({
    el: '#app',
    router: Routes,
    render: h => h(App)
});


export default app;
