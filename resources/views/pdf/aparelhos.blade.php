<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Projeto PMZ</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
<h3>Listagem de Aparelhos do Usuário: {{ $usuario->cod_pessoa }} - {{ $usuario->nome_usuario }}</h3>

<div class="tabler-responsive">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Código</th>
            <th>Descrição</th>
        </tr>
        </thead>
        <tbody>
        @foreach($usuario->aparelhos as $aparelho)
            <tr>
                <td>{{ $aparelho->codigo_aparelho }}</td>
                <td>{{ $aparelho->descricao_aparelho }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>
